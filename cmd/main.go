package main

import (
	"log"
	"os"

	"github.com/kelseyhightower/envconfig"

	apimodule "gitlab.com/bitscorp/segmed/test-task-go-segmed/modules/api"
	"gitlab.com/bitscorp/segmed/test-task-go-segmed/pkg/runtime"
)

func main() {
	if err := run(); err != nil {
		log.Printf("%+v\n", err)
		os.Exit(1)
	}
}

func run() error {
	action := os.Args[1]

	var s runtime.Spec
	if err := envconfig.Process("", &s); err != nil {
		return err
	}

	r := runtime.New(action, s)

	runtimes := []runtime.InitFunc{
		apimodule.New,
	}

	for _, m := range runtimes {
		if err := m(r); err != nil {
			return err
		}
	}

	return r.Run()
}
