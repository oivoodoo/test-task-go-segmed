# Deployment

```
heroku create --buildpack https://github.com/heroku/heroku-buildpack-go.git

git push heroku master
```

# Env variables:

```
MODULE_API_REDIS_USER=<user>
MODULE_API_REDIS_PASS=<pass>
MODULE_API_REDIS_HOST=<host>
MODULE_API_REDIS_PORT=<port>
MODULE_API_PIXABAY_KEY=<key>
ENV=prod

// On building vue project.
NODE_ENV=production
```

# Development

`make pg` or `docker-compose up -d` to start redis server

`go run cmd/main.go api`

`cd web/ ; npm run serve`

if you are making the changes for frontend, you should rebuild `npm run build`.

Open `http://localhost:8080/`

# Production

[https://segmed-images-list.herokuapp.com/index.html](https://segmed-images-list.herokuapp.com/index.html)
