module gitlab.com/bitscorp/segmed/test-task-go-segmed

go 1.14

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-redis/redis/v8 v8.2.3
	github.com/hashicorp/go-uuid v1.0.2
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.6.1
	github.com/ztrue/tracerr v0.3.0
	go.opentelemetry.io/otel v0.12.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
)
