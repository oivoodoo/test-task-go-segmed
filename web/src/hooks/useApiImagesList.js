import { ref } from 'vue';

import config from '../config/config';

export default function useApiImagesList() {
  const data = ref([]);
  const status = ref('');

  async function fetchImages(userID) {
    status.value = 'loading';
    try {
      const res = await fetch(`${config.BaseURL}/api/v1/users/${userID}/images`);
      const json = await res.json();
      status.value = 'success';
      data.value = json;

      console.log(json);
    } catch (e) {
      status.value = 'error';
    }
  }

  return {
    data,
    fetchImages,
    status,
  };
}
