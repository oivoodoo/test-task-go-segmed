const BaseURL = 'https://segmed-images-list.herokuapp.com';

const config = { BaseURL };

export default config;
