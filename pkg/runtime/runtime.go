package runtime

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/pkg/errors"
	"github.com/ztrue/tracerr"
	"go.uber.org/zap"

	"gitlab.com/bitscorp/segmed/test-task-go-segmed/pkg/logging"
)

type InitFunc func(*Runtime) error

type Spec struct {
	Env      string `envconfig:"ENV" required:"True"`
	HTTPPort int    `envconfig:"HTTP_PORT" default:"5000"`
}

func (s Spec) Dev() bool {
	return s.Env == "dev" || s.Env == "staging" || s.Env == "test"
}

type Closable interface {
	Close()
}

type Runtime struct {
	router    chi.Router
	spec      Spec
	closeCh   chan struct{}
	closables []Closable
	readyCh   chan struct{}

	errCh chan error
	dieCh chan os.Signal

	// action should determine what to do
	// types:
	// default - running web api
	action string

	Logger *zap.SugaredLogger
}

func New(action string, s Spec) *Runtime {
	r := &Runtime{
		router:  chi.NewRouter(),
		closeCh: make(chan struct{}),
		readyCh: make(chan struct{}),
		dieCh:   make(chan os.Signal),
		errCh:   make(chan error),
		spec:    s,
		action:  action,
	}

	if r.action == "api" {
		// add handlers routing
		// A good base middleware stack
		r.router.Use(middleware.RequestID)
		r.router.Use(middleware.RealIP)
		r.router.Use(middleware.Logger)
		r.router.Use(middleware.Recoverer)

		r.router.Use(cors.Handler(cors.Options{
			AllowedOrigins: []string{"*"},
			AllowedMethods: []string{"GET", "POST"},
			AllowedHeaders: []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
			MaxAge:         300,
		}))

		r.router.Use(middleware.Timeout(60 * time.Second))
	}

	return r
}

func (r *Runtime) WithStatic(path string, dataPath string) {
	workDir, _ := os.Getwd()
	root := http.Dir(filepath.Join(workDir, dataPath))

	logger := r.Logger

	r.Logger.Debugf("[router.static] path: %s, root: %s", path, root)

	r.router.Get(path, func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		logger.Debugf("[router.static] serving path prefix: %s", pathPrefix)
		fs := http.StripPrefix(pathPrefix, http.FileServer(root))
		fs.ServeHTTP(w, r)
	})
}

func (r *Runtime) WithRoutes(fn func(r chi.Router)) {
	fn(r.router)
}

func (r *Runtime) WithLogger(env, namespace string) error {
	l, err := logging.ConfigForEnv(env).Build(
		zap.Fields(zap.String("project", namespace)),
	)
	if err != nil {
		return err
	}
	r.Logger = l.Sugar()

	return nil
}

// WithClosable registers Closable resource which will be cleaned up
// upon runtime Close call. Useful for tests to avoid db connection issues.
func (r *Runtime) WithClosable(c Closable) {
	r.closables = append(r.closables, c)
}

func health(writer http.ResponseWriter, _ *http.Request) {
	writer.WriteHeader(http.StatusOK)
	writer.Write([]byte("OK"))
}

func (r *Runtime) Run() error {
	s := r.spec

	r.router.Get("/healthz", health)
	r.printRoutes()

	l, err := logging.ConfigForEnv(s.Env).Build()
	if err != nil {
		return err
	}
	defer l.Sync()
	logger := l.Sugar()

	httpListener, err := net.Listen("tcp", fmt.Sprintf(":%d", s.HTTPPort))
	if err != nil {
		return tracerr.Wrap(errors.Wrapf(err, "http listener: could not listen to :%d.", s.HTTPPort))
	}

	httpServer := &http.Server{
		Addr:    fmt.Sprintf(":%d", s.HTTPPort),
		Handler: r.router,
	}

	signal.Notify(r.dieCh, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := httpServer.Serve(httpListener); err != nil && err != http.ErrServerClosed {
			r.errCh <- errors.Wrap(err, "http server failed")
		}
	}()
	logger.Debugf("started http server on port: %d", s.HTTPPort)

	// broadcast that runtime is ready
	close(r.readyCh)

	// run runtime loop and wait for specific event
	// done or close
	select {
	case <-r.closeCh:
	case <-r.dieCh:
	case err := <-r.errCh:
		logger.Errorf("received error signal: %s", tracerr.Wrap(err))
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := httpServer.Shutdown(ctx); err != nil {
		return tracerr.Wrap(fmt.Errorf("http server shutdown failed: %+v", err))
	}
	logger.Info("stopped http server")

	return nil
}

func (r *Runtime) Close() {
	r.closeCh <- struct{}{}
	for _, c := range r.closables {
		c.Close()
	}
}

func (r *Runtime) Wait() {
	<-r.readyCh
	return
}

func (r *Runtime) printRoutes() {
	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		fmt.Printf("%s %s\n", method, route)
		return nil
	}

	if err := chi.Walk(r.router, walkFunc); err != nil {
		fmt.Printf("Logging err: %s\n", err.Error())
	}
}
