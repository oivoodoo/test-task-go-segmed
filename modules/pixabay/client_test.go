package pixabay

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Example:
// https://pixabay.com/api/?key=18546191-c4336234d042c49fe45b7fb82&q=yellow+flowers&image_type=photo
// Demo key:
const Key = "18546191-c4336234d042c49fe45b7fb82"

func TestClientRandomImages(t *testing.T) {
	client := NewClient(Config{
		Key: Key,
	})

	images, err := client.ListImages(context.Background())
	assert.Nil(t, err)
	assert.Len(t, images, 20)
}
