package pixabay

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
)

const BaseURL = "https://pixabay.com/api"

type Client struct {
	config Config
	logger *zap.SugaredLogger
}

type Image struct {
	ID         int64  `json:"id"`
	PageURL    string `json:"pageURL"`
	PreviewURL string `json:"previewURL"`
	Width      int    `json:"imageWidth"`
	Height     int    `json:"imageHeight"`
	Likes      int64  `json:"likes"`
	Tags       string `json:"tags"`
}

type Images struct {
	Hits []Image
}

func NewClient(config Config, logger *zap.SugaredLogger) *Client {
	client := &Client{config, logger}
	return client
}

/*
{
    "total":1119305,
    "totalHits":500,
    "hits":[
        {
            "id":5609713,
            "pageURL":"https://pixabay.com/photos/pumpkin-vegetables-harvest-squash-5609713/",
            "type":"photo",
            "tags":"pumpkin, vegetables, harvest",
            "previewURL":"https://cdn.pixabay.com/photo/2020/09/28/12/45/pumpkin-5609713_150.jpg",
            "previewWidth":150,
            "previewHeight":100,
            "webformatURL":"https://pixabay.com/get/53e6d54a4d53af14f1dc846096293f7b1139dfec524c704f752e7dd79445c65a_640.jpg",
            "webformatWidth":640,
            "webformatHeight":427,
            "largeImageURL":"https://pixabay.com/get/53e6d54a4d53af14f6da8c7dda793676103bd8e45a506c48732f7ad2944ecd5bba_1280.jpg",
            "imageWidth":6000,
            "imageHeight":4000,
            "imageSize":3539436,
            "views":11052,
            "downloads":10344,
            "favorites":56,
            "likes":87,
            "comments":23,
            "user_id":2364555,
            "user":"pixel2013",
            "userImageURL":"https://cdn.pixabay.com/user/2020/07/25/21-10-11-80_250x250.jpg"
		}
	]
}
*/
func (c *Client) request(ctx context.Context, path string) ([]byte, error) {
	resp, err := http.Get(fmt.Sprintf("%s/%s?key=%s", BaseURL, path, c.config.Key))
	if err != nil {
		return nil, err
	}
	c.logger.Debugf("[pixabay.client] response on request: %v", resp)

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, err
}

// ListImages returns 20 photos by default
func (c Client) ListImages(ctx context.Context) ([]Image, error) {
	images := Images{}

	data, err := c.request(ctx, "")
	if err != nil {
		return nil, err
	}

	c.logger.Debugf("[pixabay.client] data in the body: %s", string(data))

	err = json.Unmarshal(data, &images)
	if err != nil {
		return nil, err
	}

	return images.Hits, nil
}
