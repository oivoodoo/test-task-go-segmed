package service

import (
	"context"

	"go.uber.org/zap"

	"gitlab.com/bitscorp/segmed/test-task-go-segmed/modules/api/internal/models"
	"gitlab.com/bitscorp/segmed/test-task-go-segmed/modules/pixabay"
)

// RedisRepository should provide access to user properties
type RedisRepository interface {
	ImagesPin(ctx context.Context, userID string, image *models.Image) error
	ImagesPinned(ctx context.Context, userID string) ([]*models.Image, error)
}

// Service contains all dependencies to perform common service tasks.
type Service struct {
	repo RedisRepository

	client *pixabay.Client

	logger *zap.SugaredLogger
}

// NewService should build the service to having the layer between handlers
// and repositories.
func NewService(repo RedisRepository, client *pixabay.Client, logger *zap.SugaredLogger) *Service {
	return &Service{repo, client, logger}
}

func (s *Service) ImagesPin(ctx context.Context, userID string, image models.Image) error {
	image.Pinned = true

	return s.repo.ImagesPin(ctx, userID, &image)
}

func (s *Service) ImagesList(ctx context.Context, userID string) ([]*models.Image, error) {
	var images []*models.Image

	userImages, err := s.ImagesPinned(ctx, userID)
	if err != nil {
		return nil, err
	}

	for _, image := range userImages {
		images = append(images, image)
	}

	clientImages, err := s.client.ListImages(ctx)
	if err != nil {
		return nil, err
	}

	s.logger.Debugf("[service.ImagesList] received the client images: %v", clientImages)

	for _, ci := range clientImages {
		found := false
		for _, userImage := range userImages {
			if userImage.ID == ci.ID {
				found = true
				break
			}
		}

		if !found {
			images = append(images, &models.Image{
				URL:     ci.PreviewURL,
				PageURL: ci.PageURL,
				ID:      ci.ID,
				Width:   ci.Width,
				Height:  ci.Height,
				Likes:   ci.Likes,
				Tags:    ci.Tags,
				Pinned:  false,
			})
		}
	}

	return images, nil
}

func (s *Service) ImagesPinned(ctx context.Context, userID string) ([]*models.Image, error) {
	return s.repo.ImagesPinned(ctx, userID)
}
