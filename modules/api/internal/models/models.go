package models

import "strconv"

type Image struct {
	ID         int64  `json:"id"`
	URL        string `json:"url"`
	PageURL    string `json:"page_url"`
	Width      int    `json:"width"`
	Height     int    `json:"height"`
	Likes      int64  `json:"likes"`
	Tags       string `json:"tags"`
	InternalID string `json:"iid"`
	Pinned     bool   `json:"pinned"`
}

func NewImageByAttrs(attrs map[string]string) (*Image, error) {
	id, err := strconv.ParseInt(attrs["id"], 10, 64)
	if err != nil {
		return nil, err
	}
	width, err := strconv.ParseInt(attrs["width"], 10, 64)
	if err != nil {
		return nil, err
	}
	height, err := strconv.ParseInt(attrs["height"], 10, 64)
	if err != nil {
		return nil, err
	}
	likes, err := strconv.ParseInt(attrs["likes"], 10, 64)
	if err != nil {
		return nil, err
	}
	pinned, err := strconv.ParseBool(attrs["pinned"])
	if err != nil {
		return nil, err
	}

	image := &Image{
		ID:         id,
		Width:      int(width),
		Height:     int(height),
		Likes:      likes,
		Pinned:     pinned,
		URL:        attrs["url"],
		PageURL:    attrs["page_url"],
		Tags:       attrs["tags"],
		InternalID: attrs["iid"],
	}
	return image, nil
}
