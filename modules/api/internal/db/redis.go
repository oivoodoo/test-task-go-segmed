package db

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/hashicorp/go-uuid"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/bitscorp/segmed/test-task-go-segmed/modules/api/internal/models"
)

type RedisRepository struct {
	conn *redis.Client

	logger *zap.SugaredLogger
}

func NewRedisRepository(c *redis.Client, l *zap.SugaredLogger) *RedisRepository {
	return &RedisRepository{c, l.With("scope", "redis")}
}

func userKey(id string) string {
	return fmt.Sprintf("users:%s", id)
}

func imageKey(id string, imageID string) string {
	return fmt.Sprintf("users:%s:images:%s", id, imageID)
}

func (r RedisRepository) ImagesPin(ctx context.Context, userID string, image *models.Image) error {
	var err error

	pipe := r.conn.Pipeline()

	if image.InternalID == "" {
		uuid, err := uuid.GenerateUUID()
		if err != nil {
			return nil
		}

		image.InternalID = uuid
	}

	_, err = pipe.ZAdd(
		ctx,
		userKey(userID),
		&redis.Z{
			Score:  float64(time.Now().Unix()),
			Member: image.InternalID,
		},
	).Result()
	if err != nil {
		return err
	}

	var values []string
	values = append(values, "id", strconv.FormatInt(image.ID, 10))
	values = append(values, "iid", image.InternalID)
	values = append(values, "url", image.URL)
	values = append(values, "page_url", image.PageURL)
	values = append(values, "width", strconv.FormatInt(int64(image.Width), 10))
	values = append(values, "height", strconv.FormatInt(int64(image.Height), 10))
	values = append(values, "likes", strconv.FormatInt(int64(image.Likes), 10))
	values = append(values, "tags", image.Tags)
	values = append(values, "pinned", strconv.FormatBool(image.Pinned))

	r.logger.Debugf("set image attributes: %v", values)

	_, err = pipe.HSet(ctx, imageKey(userID, image.InternalID), values).Result()
	if err != nil {
		return err
	}

	_, err = pipe.Exec(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (r RedisRepository) ImagesPinned(ctx context.Context, userID string) ([]*models.Image, error) {
	var err error
	var images []*models.Image

	// begin get ids
	pipe := r.conn.Pipeline()
	pipe.ZRange(ctx, userKey(userID), 0, -1)

	r.logger.Debugf("[repo] begin zrange for user id: %s", userID)

	results, err := pipe.Exec(ctx)
	if err != nil {
		return nil, err
	}

	r.logger.Debugf("[repo] done zrange for user results: %v", results)

	if len(results) == 0 {
		return []*models.Image{}, nil
	}

	if imagesSlice, ok := results[0].(*redis.StringSliceCmd); !ok {
		return nil, errors.New("wrong type return from redis sorted set")
	} else {
		r.logger.Debugf("[repo] begin parse images slize", results)
		imagesResult, err := imagesSlice.Result()
		if err != nil {
			return nil, err
		}

		var ids []string
		for _, id := range imagesResult {
			ids = append(ids, id)
		}

		r.logger.Debugf("[repo] done parse images slice %v", ids)

		pipe = r.conn.Pipeline()

		for _, id := range ids {
			pipe.HGetAll(ctx, imageKey(userID, id))
		}

		results, err = pipe.Exec(ctx)
		if err != nil {
			return nil, err
		}

		for _, res := range results {
			if cmd, ok := res.(*redis.StringStringMapCmd); ok {
				attrs, err := cmd.Result()
				if err != nil {
					err = errors.WithMessage(err, "can't get other user image")
					r.logger.Error(err)
					return nil, err
				}
				if len(attrs) == 0 {
					continue
				}

				r.logger.Debugf("get image attributes: %v", attrs)

				image, err := models.NewImageByAttrs(attrs)
				if err != nil {
					return nil, err
				}

				images = append(images, image)
			}
		}
	}

	return images, nil
}
