package handlers

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/bitscorp/segmed/test-task-go-segmed/modules/api/internal/models"
	"gitlab.com/bitscorp/segmed/test-task-go-segmed/modules/api/internal/service"
	httpreq "gitlab.com/bitscorp/segmed/test-task-go-segmed/pkg/http"
)

type Handler struct {
	service *service.Service

	logger *zap.SugaredLogger
}

func New(s *service.Service, l *zap.SugaredLogger) *Handler {
	return &Handler{
		service: s,
		logger:  l.With("scope", "api"),
	}
}

func (h *Handler) ImagesPin(w http.ResponseWriter, r *http.Request) {
	var err error

	userID := chi.URLParam(r, "user_id")

	h.logger.Debugf("[api.ImagesPin] begin user id: %s", userID)

	image := models.Image{}
	err = httpreq.Read(r, &image)
	if err != nil {
		h.logger.Errorf("[api.ImagesPin] error: %s", err.Error())
		httpreq.Error(w, errors.Wrap(err, "can't read pin image request"))
		return
	}

	h.logger.Debugf("[api.ImagesPin] user id: %s, pin image: %v", userID, image)

	err = h.service.ImagesPin(r.Context(), userID, image)
	if err != nil {
		httpreq.Error(w, errors.Wrap(err, "can't upsert leaderboard data"))
		return
	}

	httpreq.OK(w)
}

func (h *Handler) ImagesList(w http.ResponseWriter, r *http.Request) {
	userID := chi.URLParam(r, "user_id")

	h.logger.Debugf("[api.ImagesList] user_id: %s", userID)

	images, err := h.service.ImagesList(r.Context(), userID)
	if err != nil {
		httpreq.Error(w, errors.Wrap(err, "can't get list of images"))
	}

	httpreq.JSON(w, images)
}
