package auth

import (
	"github.com/go-chi/chi"
	"github.com/go-redis/redis/v8"
	"github.com/kelseyhightower/envconfig"

	"gitlab.com/bitscorp/segmed/test-task-go-segmed/modules/api/internal/db"
	"gitlab.com/bitscorp/segmed/test-task-go-segmed/modules/api/internal/handlers"
	"gitlab.com/bitscorp/segmed/test-task-go-segmed/modules/api/internal/service"
	"gitlab.com/bitscorp/segmed/test-task-go-segmed/modules/pixabay"
	redisconf "gitlab.com/bitscorp/segmed/test-task-go-segmed/pkg/redis"
	"gitlab.com/bitscorp/segmed/test-task-go-segmed/pkg/runtime"
)

type spec struct {
	Env     string           `envconfig:"ENV" required:"True"`
	Redis   redisconf.Config `envconfig:"REDIS" required:"True"`
	Pixabay pixabay.Config   `envconfig:"PIXABAY" required:"True"`
}

func New(r *runtime.Runtime) error {
	var s spec
	if err := envconfig.Process("MODULE_API", &s); err != nil {
		return err
	}
	return withSpec(r, s)
}

func withSpec(r *runtime.Runtime, s spec) error {
	r.WithLogger(s.Env, "modules/api")

	logger := r.Logger
	client := pixabay.NewClient(s.Pixabay, logger)
	redisConn := redis.NewClient(s.Redis.Options())
	repo := db.NewRedisRepository(redisConn, logger)
	svc := service.NewService(repo, client, logger)
	h := handlers.New(svc, logger)

	r.WithStatic("/index.html", "web/dist")
	r.WithStatic("/css/*", "web/dist/css")
	r.WithStatic("/js/*", "web/dist/js")

	r.WithRoutes(func(r1 chi.Router) {
		r1.Post("/api/v1/users/{user_id}/images/pin", h.ImagesPin)
		r1.Get("/api/v1/users/{user_id}/images", h.ImagesList)
	})

	return nil
}
