current_dir = $(shell pwd)

DOCKER_REGISTRY=registry.gitlab.com/oivoodoo/test-task-go-segmed
DOCKER_COMPOSE=pipenv run docker-compose

init:
	pip install --user pipenv
	pipenv install
	cd deploy/ansible/ ; pipenv install ; cd ../../
.PHONY: init

up:
	${DOCKER_COMPOSE} up -d
.PHONY: up

stop:
	${DOCKER_COMPOSE} stop
.PHONY: stop

rm:
	${DOCKER_COMPOSE} rm -y
.PHONY: rm

lint:
	docker run --rm -v $(current_dir):/app -w /app golangci/golangci-lint:v1.24.0 golangci-lint run -v ./...
.PHONY: lint

release:
	docker build -t $(DOCKER_REGISTRY):latest .
	docker push $(DOCKER_REGISTRY):latest
.PHONY: release

deploy:
	echo 'TODO'
.PHONY: deploy
